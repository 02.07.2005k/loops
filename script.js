"use strict"
// Туоретичні Питання 
// 1. Цикл в програмуванні - це конструкція, 
// яка дозволяє виконувати певні дії кілька разів, поки виконується певна умова.

// 2. В JavaScript є три основних види циклів:
//    - for: цикл, який виконується певну кількість разів,
//      вказану в передумові, ітеруючи по числовому діапазону.
//    - while: цикл, який виконується, доки вказана умова є істинною. 
//      Умова перевіряється перед кожним виконанням циклу.
//    - do...while: цикл, який виконується, доки вказана умова є істинною. 
//      Умова перевіряється після кожного виконання циклу.

// 3. Цикл do...while відрізняється від циклу while тим, 
// що умова перевіряється після кожного виконання циклу. 
// Це означає, що код у блоку циклу do...while буде виконуватися принаймні один раз, 
// навіть якщо умова вже не виконується. 
// У циклі while умова перевіряється перед виконанням циклу, 
// тому цикл може взагалі не виконатися, якщо умова вже не виконується з початку.

// Практичне завдання 1
function isNumber(value) {
    return !isNaN(parseFloat(value)) && isFinite(value);
}

function getNumberFromUser(promptMessage) {
    let userInput;
    do {
        userInput = prompt(promptMessage);
    } while (!isNumber(userInput));
    return parseFloat(userInput);
}

let number1 = getNumberFromUser('Введіть перше число:');
let number2 = getNumberFromUser('Введіть друге число:');

let minNumber = Math.min(number1, number2);
let maxNumber = Math.max(number1, number2);

let message = `Числа від ${minNumber} до ${maxNumber}:\n`;
for (let i = minNumber; i <= maxNumber; i++) {
    message += i + '\n';
}
alert(message);

// Практичне завдання 2
function isEven(number) {
    return number % 2 === 0;
}

function getEvenNumberFromUser(promptMessage) {
    let userInput;
    do {
        userInput = prompt(promptMessage);
        if (!isEven(userInput)) {
            alert('Введене число не є парним. Будь ласка, введіть парне число.');
        }
    } while (!isEven(userInput));
    return parseInt(userInput);
}

let evenNumber = getEvenNumberFromUser('Введіть парне число:');

alert(`Ви ввели парне число: ${evenNumber}`);